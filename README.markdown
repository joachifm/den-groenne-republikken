# Frihet og grønn politisk teori
En artikkel om frihet og grønn politisk teori, spesifikt
hvordan frihet som fravær av herredømme kan i bidra til
utvikling av grønn politisk teori som ikke innebærer retrett fra
individualisme og pluralisme (i motsetning til hva som er tilfelle
for teorier som implisitt tar utgangspunkt i negativ frihet).

Artikkelen ble opprinnelig skrevet som en semesteroppgave i
STV4122 ved Universitetet i Oslo, våren 2011.
Den publiseres som et eksempel til skrekk og advarsel, i håp om at
mine feilsteg kan komme andre til nytte.

Teksten kan distribueres fritt, på det vilkår at eventuelle endringer
tydelig fremgår og tillegges riktig vedkommende.
Se [Creative Commons Attribution-ShareAlike] for mer spesifikke
betingelser (se også `COPYING_CCPL` i kildetreet).

Dokumentkilden er tilgjengelig under [LPPL] (se `COPYING_LPPL` i kildetreet).

For å bygge dokumentet kreves en fungerende installasjon av [LaTeX].

[LPPL]: http://latex-project.org/lppl.txt
[Creative Commons Attribution-ShareAlike]: https://creativecommons.org/licenses/by-sa/3.0/
[LaTeX]: http://latex-project.org/
