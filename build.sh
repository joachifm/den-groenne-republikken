#!/bin/sh

out="./out"
docroot="./den_groenne_republikken"
name="den-groenne-republikken"

mkdir -p "$out"
git describe --always > ./.version
latexmk -silent -output-directory="$out" -pdf -jobname="$name" "$docroot" &> "$out"/latexmk.log
