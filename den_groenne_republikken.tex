\documentclass[a4paper]{article}

\usepackage{mystyle}

\begin{document}

\input{frontpage}
\maketitle
\tableofcontents
\newpage

\section{Innledning}
\label{sec:innledning}

Å forene humanistiske kjerneverdier som autonomi og pluralisme med økologiske
hensyn, er en vanskelig oppgave for etablert politisk teori.
Tradisjonelle politiske ideologier er naturligvis menneskesentrerte og derfor
ikke umiddelbart overførbare til de unike etiske og politiske dilemmaene som
oppstår i skjæringspunktet mellom menneskelige behov og økosystemers bæreevne;
de kan i utgangspunktet ikke si oss noe om hvilke behov som er legitime
vis-a-vis naturen eller hva det konkret innebærer å ta hensyn til den.
Forsøk på å innpasse økologiske hensyn i disse teoriene
\citep{dobson06,dobson07} er bare delvis vellykkede.
Dette kan i mange tilfeller føres tilbake til en gitt måte å forstå frihet på.

Dersom frihet forstås som fravær av fysisk tvang, eller troverdige trusler
om fysisk tvang \citep{pettit99}, vil ethvert inngrep i enkeltpersoners
valgspekter med nødvendighet innebære en reduksjon i deres frihet.
Den eneste legitime måten å argumentere for redusert frihet er med
henvisning til andres frihet (i den forstand at min frihet slutter der din
begynner).
Et negativt frihetsbegrep vil, trukket til sitt logiske ytterpunkt,
medføre en vulgær hedonisme, hvor den enkeltes frihetsutøvelse kun
begrenses i den grad den kommer i konflikt med andres frihet.
Dette er åpenbart utilstrekkelig som normativ rettesnor: implikasjonene
det har i ren form er uforenlige med økologiske hensyn.

Samtidig er det denne forståelsen av frihet som ofte legges til grunn i
konvensjonell politisk teori --- argumentasjonen, enten for eller mot,
refererer alltid til frihet som fravær av hindringer.
Det er naturlig da, at liberalismens forsøk på å forene negativ frihet og
økologiske verdier \citep{wissenburg06} ender opp i et komplekst system
av støttepremisser som i mange tilfeller vil være umulig å anvende i
praksis.
Like naturlig er det at flere teoretikere svarer med å nedvurdere
betydningen av individuell frihet, slik man ser i kollektivistiske
ideologier som konservatisme \citep{scruton06} og kommunitarisme
\citep{eckersley06}.
Begge måtene å løse frihetsparadokset svekker teorienes anvendbarhet som
utgangspunkt for politisk argumentasjon og handling.

Av det overstående kan sluttes (1) at etablert humanistisk teori må
forkastes \citep{naess05}; eller (2) at dette er ``partielle teorier''.
Den første slutningen er lite tilfredsstillende, da enhver plattform for
politisk handling med nødvendighet må ta utgangspunkt i mennesket og favne
et bredt spekter av interesser og verdier \citet{barry99}.
En seriøs grønn politisk teori må ta menneskelige behov, og da i sær
personlig frihet, på alvor.
Videre må den kunne virke som utgangspunkt og rettesnor for institusjonell
utforming, som ikke bryter fullstendig med etablert politisk praksis: den
må kunne skissere en realistisk vei fra status quo til en tilnærmet
idealsituasjon.
For å nå dette målet er det en fordel å ha en teori bygget på ett bærende
ideal, som både kan begrunne omfanget av individuell frihet og måten
statens institusjoner er utformet.

Nyrepublikanisme \citep{pettit99} er en kandidat til en slik teori.
Her er frihet, definert som fravær av herredømme, det grunnleggende aksiomet
som alt annet flyter ut av: frihet som ikke-herredømme er alle tings mål og
målestokk.
\citet{pettit99} argumenterer for at dette er et frihetsideal som burde
være tiltrekkende også for moderne mennesker, uavhengig av hva annet de
måtte ønske seg, at republikanisme som politisk filosofi langt fra har
utspilt sin rolle, og at en stat som fremmer dette idealet også vil
ivareta de fleste andre kjerneverdier som eksplisitt eller implisitt
ligger til grunn for moderne samfunn.
Miljø- og klimaproblematikken vies derimot ikke særlig oppmerksomhet.
Jeg ønsker å videreføre \citeauthor{pettit99}s resonnement og vise at
hans slutninger også gjelder for \emph{grønn} politisk teori: at republikanisme
har mye å bidra med og er i stand til å møte den ``økologiske utfordringen''
\citep{dobson06}.

Det er naturlig å følge \citeauthor{pettit99}s inndeling i (1) republikanisme
som individuelt og kollektivt attraktivt ideal; og (2) republikkens
institusjonelle virkemåte.  Innholdet i punkt (2) avhenger av (1), og hvis (1)
ikke er ønskverdig, er (2) irrelevant.
Jeg vil, i forlengelsen av \citet{pettit99} argumentere for at frihet som
ikke-dominans er et egnet utgangspunkt for en grønn etikk, uten mange av de
interne og eksterne motsetningene andre teorier plages av.
Formålet er å sannsynliggjøre at det er et teoretisk grunnlag for en grønn
republikk og at en stat som fremmer idealet om frihet som ikke-herredømme kan
være ``økologisk rasjonell'' \citep{barry99}, men jeg vil begrense meg til å
antyde heller enn gi en fullstendig beskrivelse av hvordan en slik republikk kan se ut.

\section{Frihet som ikke-dominans}
\label{sec:ikkedominans}

\subsection{Frihet som kjerneverdi}

En grønn politisk teori kan vurderes fra minst to synsvinkler: som grunnlag
for handling og som konstruktiv politisk filosofi.
Vurdert som utgangspunkt for handling må teorien gjøre det mulig å begrunne gitte
sosiale ordninger, fordeling av byrder og goder, rettigheter og plikter,
overfor enkeltpersoner- og grupper på en måte som gir grunn til å slutte opp
om dem.
Uten dette er utsiktene for definering av omforente målsetninger og
hvilke virkemidler det er legitimt å benytte for å nå dem, små.
En konstruktiv politisk filosofi må dessuten si noe om hvilke politiske
institusjoner som er nødvendige og beskrive deres utforming og virkemåte, fra
det abstrakte til det konkrete og trivielle.

Å forutsette at frihet er den grunnleggende verdien grønn politisk teori må
bygge på, kan virke kontraintuitivt, all den tid det grønne prosjektet har som
mål å fremme økologiske eller biosentriske verdier.
Frihet er et bedre utgangspunkt fordi alle andre humanistiske verdier kan
reduseres til eller utledes fra dette ene konseptet \citep{pettit99}.
Uten frihet blir de andre verdiene hule.
Antroposentrisk filosofi er med andre ord nødt til å ta hensyn til frihet.

Politiske teorier som ikke tar frihet på alvor, eller som ikke klarer å forene
frihet og økologi på en fornuftig måte, er per definisjon uattraktive.
Argumentasjon med utgangspunkt i en slik teori vil ha begrenset
overbevisningskraft og dertil svekket politisk betydning.
Motsatt vil det å vise hvordan disse hensynene kan forenes, og helst hvordan det
ene følger av det andre, legge opp til argumentasjon som i det minste er mer
tilfredsstillende rent logisk.

Hva som skal legges i begrepet er derimot ikke gitt, men uansett vil det ha stor
betydning for hvilke resonnementer det er mulig å konstruere innenfor et gitt
filosofisk system (dvs., ideologi).
Hvis man slutter at frihet kan lede til normativt uholdbare utfall i konkrete
situasjoner, er det kanskje nødvendig å revurdere hvilken vekt individuell
frihet skal tillegges vis-a-vis andre hensyn, som i sin tur kan få konsekvenser
for hvilke institusjonelle ordninger som fremstår som mest passende.
Ringvirkningene kan variere avhengig av hvilken stilling frihet har i
utgangspunktet, om det har status som primærgode eller ikke, for eksempel.
Dette kan illustreres ved å se på forsøk på å innpasse økologisk tankegods i
etablerte politiske teorier som liberalisme og konservatisme.

\citet{wissenburg06} begynner med en anerkjennelse av at
nøytralitetsprinsippet, idéen om å maksimere mulige livsplaner for hver enkelt,
gjør klassisk liberalisme dårlig utstyrt til å møte den økologiske utfordringen.
Nøytralitetsprinsippet står i direkte motsetning til behovet for en mer
skånsom bruk av naturressurser og tiltak for å hindre irreversible
klimaendringer, fordi dette uvegerlig må innebære at visse livsplaner
utelukkes fra settet av tilgjengelige livsplaner \citep[22]{wissenburg06}.
Med utgangspunkt i negativ frihet er det åpenbart klare grenser for hvordan
slike begrensninger kan begrunnes: ``The inevitable result is that negative
liberty can no longer be seen as the supreme criterion of a good society''
\citep[31]{wissenburg06}.

Kjernen i problemet er at begrensninger utover Mills ``no
harm''-prinsipp \citep[26]{wissenburg06} i utgangspunktet ikke er mulig.
Begrensningene kan være vel begrunnede eller ikke, men de må begrunnes med
referanse til andre verdier enn frihet som sådan, for eksempel idéen om
likeverd og naturlige rettigheter, som i den grad de anerkjennes kan brukes
som legitime argumenter for å moderere frihetsutøvelse.
Like fullt er disse moderasjonene å regne som tap av frihet for noen.
% XXX: bedre formulering her?
I ren form er negativ frihet et paradigme hvor alle endringer i fordelingen
av frihet utgjør et nullsumspill \citep{pettit99}.
Det vil være en evig ambivalens til alle former for begrensninger.

En utvidelse av ``moralsk relevante subjekter'' er en nærliggende løsning, da det
ville gi ``no harm''-prinsippet tilsvarende utvidet gyldighet, men dette er et
absurd foretak.
For det første er det vanskelig å komme frem til moralsk relevante attributter
som rimeligvis også kan tilskrives ikke-menneskelige systemer.
Selv om det lot seg gjøre vil alle forsøk på å definere moralske hierarkier
bryte sammen fordi det alltid vil være grenser for hvor langt slike
resonnementer kan strekkes (se \citet[27--28]{wissenburg06}), de vil aldri
omfatte hele naturen eller et tilstrekkelig stort subsett av den.

Som hos Rawls er det en fare for å drives fra skanse til skanse, hvor man stadig
må innføre nye tilleggspremisser (se \citet[23--26]{wissenburg06}) for å få
systemet til å henge sammen.
Hvert tilleggspremiss øker mulighetene for interne motsetninger som kan føre til
at det i konkrete beslutningssituasjoner blir umulig å utlede hva som er riktig
praksis.
Det er dessuten vanskelig å se hvordan dette kan forenes med målsetningen om et
``minimalt normativt fundament med maksimal oppslutning''
\citep[22]{wissenburg06}.

I konservativ tenkning er den økologiske krisen forstått som en følge av
menneskelige drifter og appetitt \citep{scruton06}, og løsningen består i å
fremelske en vilje til å forsake egne behov av hensyn til omgivelsene og
fremtidige generasjoner, noe som best oppnås i mindre fellesskap.
For mye frihet, implisitt forstått som fravær av tvang, er hovedproblemet fordi
det gir menneskenaturen for stort spillerom, med alt det innebærer.
Dette er en naturlig konklusjon, gitt en negativ definisjon av frihet.
Frihet per se er viktig, men ikke et mål i seg selv og kan derfor underordnes
andre hensyn: ``freedom is not the sole or the true goal of politics''
\citep[8]{scruton06}.

Å nedvurdere betydningen av individuell frihet og/eller foreskrive
institusjonelle og sosiale ordninger som vil innebære en retrett fra sentralmakt
og nasjonale beslutningsarenaer er en strategi som går igjen i flere ideologier
(bl.a. \citet{eckersley06}).
Til grunn for slike resonnementer ligger antakelser om enkeltindividers
beveggrunner som tilsier at disse alltid vil stå i motsetning til det som er
økologisk rasjonelt, at dette er impulser som må temmes av kompakte sosiale
strukturer og lojalitetsbånd til konkrete eller abstrakte forestillinger om det
nære og kjære (se \citet{eckersley06,scruton06}).
Hvis frihet virkelig er alle gode tings opphav, er dette strategier som vil
innebære fundamentale brudd med status quo.

Måten frihet er definert på her gjør vellykket konstruksjon av en
teori som kan løse spenningen mellom individuell frihet og økologiske hensyn
lite sannsynlig.
Hvis vi ønsker å holde fast ved frihet som aksiom, er et annen frihetsbegrep
nødvendig, og det er her republikanismens fremste bidrag til grønn politisk
teori ligger.

\subsection{Frihet og herredømme}

Frihet definert som ikke-dominans tar utgangspunkt i herre-slave-relasjonen
\citep[31]{pettit99}.
Det som kjennetegner dette forholdet er ikke først og fremst at slavens
valgspekter er begrenset.
En slave kan nyte mer eller mindre frihet, forstått som fravær av hindringer,
fra dag til dag eller fra eier til eier, men det som alltid er konstant er at
eieren har anledning til å gripe inn i slavens liv \emph{på vilkårlig basis}.
Nyter slaven relativt stor frihet, i form av privilegier eller fritak fra visse
arbeidsoppgaver, skjer dette alltid på eierens premisser.
Privilegier kan gis og tas vekk etter eierens forgodtbefinnende: slaven kan
behandles godt eller dårlig, men er alltid dominert \citep[32]{pettit99}.
Vissheten om dette gjør at slaven alltid må fatte beslutninger med referanse til
herrens disposisjoner.
I motsetning til herren kan hun aldri velge fritt, uavhengig av innslaget av
reell tvang (vulgær maktutøvelse).
Det sentrale ved denne relasjonen er at den sterke alltid har \emph{anledning} til å
gripe inn, ikke at inngrep faktisk skjer.

Mer allment er herredømme en reell evne til å gripe inn, uten omkostninger
og etter eget forgodtbefinnende, i en eller flere av en annens
valgsituasjoner \citep[578]{pettit96}.
De mest åpenbare inngrepene er de som innebærer fysisk tvang eller
trusler, uttalte eller underforståtte, om negative sanksjoner.
En mer subtil form for herredømme er manipulasjon av forventet eller reell
nytte forbundet med ulike utfall, noe som kan oppnås ved hjelp av
strategisk feilfremstilling av fakta, agendafiksing og forhåndspåvirkning
av handlingsutfall \citep[578--579]{pettit96}.

En relasjon preget av dominans kjennetegnes av at det er en
ressursasymmetri mellom herre og subjekt.
Den dominerende part har et overtak som kan omsettes i krav, tvang, eller manipulasjon.
Dette er den materielle betingelsen for maktutøvelse, men frihet som
ikke-dominans vektlegger også makt som psykososialt fenomen.
I en asymmetrisk relasjon er det gode grunner til å forvente at alle
parter vil være klar over sin relative styrke vis-a-vis de øvrige partene
\citep[59]{pettit99}.
Dersom styrkeforholdet er åpent kjent vil dette føre til en type implisitt
underkastelse.
Med bevisstheten om egen svakhet følger en automatisk tilpasning i retning av
den sterkes (antatte) preferanser.
Den svake part vil aldri kunne se den sterke i øynene, aldri handle autonomt
\citep[61]{pettit99}.

Herredømme kan variere med hensyn til omfang og intensitet.
Ved total dominans, som herre-slave-eksempelet illustrerer, er alle
valgsituasjoner fullstendig underlagt en annens vilkårlige styring.
Slike relasjoner er uvanlige, og som regel vil det være snakk om delvis
dominans, hvor en aktør nyter herredømme over en annen på noen områder,
men ikke på andre \citep[580--581]{pettit96}.
Med svak institusjonell beskyttelse av arbeidstakeres rettigheter vil en
arbeidskjøper kunne dominere sine ansatte på arbeidsplassen, men ikke i
deres private hjem, for eksempel.

I republikansk tenkning er det å leve på en annens nåde, lik en slave, den
ytterste form for ufrihet \citep[32]{pettit99}.
Det som kjennetegner frihet er å være sikret mot andres vilkårlige vilje.
Frihet som ikke-dominans er et relasjonelt frihetsideal, som beskriver en
tilstand hvor ``[\ldots] you live in the presence of other people but at the
mercy of none'' \citep[80]{pettit99}.
Selv om det kan finnes sterkere aktører som i utgangspunktet er i stand
til å tvinge sin vilje på deg vil det være skranker som hindrer dem fra å
gjøre dette \citep[272]{pettit99}.

Under frihet som ikke-dominans har hver enkelt garanterte ressurser de kan
mobilisere i møte med presumptivt mektigere aktører, en form for motmakt
\citep{pettit96} som veier opp for eventuelle ressursasymmetrier.
På denne måten begrenses de best stiltes anledning til å omsette materielle
eller andre fordeler i maktutøvelse.
En aktør som har motmakt er ikke lenger prisgitt forsynet, men har en viss
kontroll over sin egen fremtid \citep[589]{pettit96}.
Denne motmakten er utelukkende ``defensiv'' og kan i seg selv ikke omsettes
i herredømme; å tilføre en svak aktør ressurser som gjør vedkommende i stand til
å motstå herredømme gir en netto positiv gevinst.
I den forstand er omfordeling av makt ikke et nullsumspill
\citep[588]{pettit96}.

I motsetning til det negative frihetsidealet måles ikke frihet forstått
som fravær av herredømme i antall nominelle valgmuligheter, men langs to
akser: intensitet og utbredelse.
Et individs frihet varierer i intensitet alt ettersom hun kan velge fritt
(uten herredømme) innenfor sitt nåværende valgspekter.  I utbredelse vil
frihet variere med antallet slike ikke-dominerte valgmuligheter
\citep{pettit96}.
Ettersom det ikke er inngrep som sådan, men \emph{vilkårlige} inngrep som
er frihetens fremste fiende, må ikke begrensninger av antall
valgmuligheter nødvendigvis forstås som reduksjon i frihet.

\section{Etiske og politiske implikasjoner}

Frihet som ikke-dominans har flere interessante etiske og politiske
implikasjoner, som alle som slutter opp om idealet må ta innover seg.
Den viktigste er at frihet er et gjensidig betinget fenomen, det er ikke noe
\emph{jeg har}, men noe som oppstår i et samfunn hvor alle borgere nyter en
tilnærmet lik grad av institusjonelt forankret sikkerhet mot vilkårlige inngrep.

\subsection{Gjensidig betinget frihet}

Dersom \emph{jeg} unngår herredømme mens du ikke gjør det, må jeg slutte at min
frihet i siste instans skyldes tilfeldigheter.
Uansett hvor store ressurser jeg rår over kan forsynet snu, og i så fall vil jeg
være prisgitt de samme kreftene som deg \citep{pettit99}.
Så lenge du domineres må jeg slutte at jeg også kan bli dominert: i siste
instans er jeg fri bare i den utstrekning du er fri.
En som slutter opp om frihet som ikke-dominans kan ikke slå seg til ro med å
selv for en tid være fri for innblanding, så lenge det er andre som ikke er like
heldige.
Å slutte opp om frihet som ikke-dominans innebærer en anerkjennelse av at enhver
form for dominans er uforenlig med et fritt samfunn: et samfunn som tolererer
dominans er et ufritt samfunn.

Koblingen mellom egen og andres frihet er ikke bare av interesse for hver enkelt
borger, men er en grunnleggende forutsetning for republikkens eksistens
overhodet.
I en situasjon hvor dominans er utbredt, hvor din frihet er avhengig av dine
personlige ressurser, har alle god grunn til å forsøke å bedre egen posisjon
vis-a-vis andre.
Denne situasjonen er i prinsippet identisk med en hobbesiansk naturtilstand
\citep[94]{pettit99}.
Et samfunn hvor herredømme eksisterer er ikke bare ufritt, det vil undergrave
seg selv.

Frihet som ikke-dominans er en bro mellom det individuelle og det kollektive: i det
å ivareta egen frihet ligger en impuls til å fremme frihet mer allment.
Å fremme frihet som ikke-dominans er en individuelt og kollektivt rasjonell
målsetning.

\subsection{Borgere og borgerånd}

Borgerbegrepet er nært forbundet med den republikanske tradisjonen.
Vanligvis tillegges borgerrollen plikter som tilsier at private preferanser må
komme i bakgrunnen til fordel for det felles gode, og at hver enkelt blir borger
i det han eller hun faktisk er i stand til å etterleve dette idealet.
Dette er en dydsetisk tankegang, hvor en god borger handler ut fra hva som er
riktig og ikke hva som er opportunt \citet[134]{dobson07}.

Denne type borger har en vesentlig plass i \citet{barry99}s reformulering av
grønn politisk teori, hvor mye av grunnlaget for institusjonell reform ligger i
en storskala overgang fra konsument til borger.
Utgangspunktet for dette er samtale på visse vilkår, som frikobler den enkelte
deltaker fra sine egne materielle interesser og gjør vedkommende i stand til å
inngå i kollektive resonnementer med henblikk på å formulere god politikk som
fremmer det felles beste.

Denne måten å forstå borgerrollen forutsetter en viss type mennesker, som
i utgangspunktet er velvillig innstilt overfor andre, som er villige til å
anerkjenne fornuften i det beste argumentet og sette egne hensyn til side.
Borger er noe man er i kraft av sin innstilling, i kraft av å ha
``borgerånd'', men det er uklart hvorvidt dette er en individuelt rasjonell
målsetning, noe som svekker realismen i \citet{barry99}s resonnement.
Så lenge borgeren forutsettes å handle på tross av egne interesser og ikke i
forlengelsen av dem, er man \emph{avhengig av} at folk ``[\ldots] somtimes act
to `do good' as well as to try to ensure some gain for themselves''
\citep[134]{dobson07}.

Dersom det å ivareta egen frihet derimot bare kan skje på vilkår av at
andres frihet også ivaretas, er altruisme ikke lenger en nødvendig
forutsetning for at enkeltindivider skal overta borgerrollen, det følger
naturlig av å slutte opp om og ta på alvor implikasjonene av frihet som
ikke-dominans.
Dette betyr at borgerånd i seg selv ikke er en \emph{konstituerende}
betingelse for republikken eller dens institusjoner.

I republikansk tenkning henviser begrepene ``borger'' og ``frihet'' til samme
fenomen \citep[36]{pettit99}: å være en borger er å være fri, å være fri
er å være en borger.
Bare som borger kan du møte andre mennesker som jevnbyrdig, og bare som
borger kan du være sikker i din frihet og fatte beslutninger på fri og
uavhengig basis.
Å bli borger er derimot ikke noe hver enkelt kan oppnå på egen hånd.
Så lenge frihet ikke kan være avhengig av de ressurser hver enkelt i
utgangspunktet har til disposisjon, følger det at borgeren bare kan bli
til i et institusjonelt rammeverk av en viss type, et lovstyrt system som
garanterer et visst minimum av motmakt.
Staten og dens lover er ikke bare nødvendige onder som vi aksepterer fordi
det, tross alt, fører til mindre tvang enn i et rent anarki \citep[35]{pettit99}.
Lovverket er en nødvendig, konstituerende, forutsetning for frihet overhodet:
``As the laws create the authority that rulers enjoy, so the laws create the
freedom that citizens share'' \citep[36]{pettit99}.

\subsection{Lovenes funksjon og virkemåte}

Enhver institusjonell forordning, enhver lov, må vurderes ut fra idealet om
frihet som ikke-herredømme.
Et relevant kriterium er hvorvidt og i hvilken utstrekning institusjonen virker
inn i borgeres liv uten å ta hensyn til deres interesser, uten å kunne
rettferdiggjøre inngrepet overfor den som utsettes for det.
Idealinstitusjonen bærer i seg det felles gode og har klare og forutsigbare
grenser for hva som er legitim innblanding: den er ``controlled by the interests
and opinions of those affected, being required to serve those interest in a way
that conforms with those opinions'' \citep[35]{pettit99}.
Disse interessene må ikke nødvendigvis tas til følge, men de må i alle tilfeller
tas hensyn til.

En god lov dominererer ikke, den er å regne som en ``naturlig hindring''
\citep{pettit99}.
Lik en stein som blokkerer veien du går på begrenser loven din bevegelsesfrihet,
men det er åpenbart urimelig å si at det foreligger noen villet forverring av
din valgsituasjon.
Dårlige institusjoner kan derimot pervertere lovene og gjøre dem til
instrumenter for å fremme enkeltpersoner- eller gruppers særinteresser.
I så fall kan loven med rette sies å være en form for herredømme
\citep[587]{pettit96}.
Et allment prinsipp er at lover og institusjoner må bindes av de interessene de
er ment å representere. Altså må det finnes mekanismer som kobler lovutforming
og borgeres behov sammen.

\subsection{Deltakelse og demokrati}

Demokratisk deltakelse per se er ikke et bærende ideal i republikansk
tenkning: ``[it] may be essential to the republic, but that is
\emph{because it is necessary for promoting the enjoyment of freedom as non-domination}''
(\citet[8]{pettit99}, min utheving).
Langt viktigere enn deltakelse i valg til representative forsamlinger er
adgangen til å gjøre kritikk og misnøye kjent, adgangen til skranker som
begrenser statens innflytelse til de sfærer som er tilkjent den av lovene
\citep[294--297]{pettit99}.
Disse skrankene må bygges inn i prosedyrene som ligger til grunn for kollektive
beslutninger.
Dette er betingelser som kommer \emph{før} deltakelse, de er nødvendige
forutsetninger for (fri) handling overhodet.

I likhet med deltakelse er deliberasjon muligens en nødvendig institusjonell
mekanisme, men også her er det lett å se at dette bare kan skje på et
eksisterende fundament.
En viktig utfordring for utforming av deliberative institusjoner er å sikre lik
adgang til samtalen.
Dette kan løses ved å konstruere regler som på en ikke-ekskluderende måte binder
deltakerne til en omforent standard for legitim argumentasjon
\citep[57]{smith03}.
I republikansk perspektiv er dette utilstrekkelig: herredømme besmitter alle
mellommenneskelige forhold og derfor må hver deltaker komme til samtalen som
allerede jevnbyrdig.
En kvinne i et samfunn som ikke respekterer kvinners rettigheter kan gjerne
delta i en samtale, bundet av regler for åpen og rimelig argumentasjon, men
dersom hun ikke også er fri fra herredømme utenfor samtalens rammer kan hun
heller aldri fremstå som en reell samtalepartner.
Deliberative mekanismer er fullt ut forenlige med republikanisme og kanskje
også nødvendige for å virkeliggjøre dens målsetninger, men borgerrollen blir til
i kraft av loven, ikke av reglene som binder samtalen.
Vi blir borgere i staten og derved kan vi samtale som likemenn.

\section{Grønn republikanisme}

Republikanismen tar bare hensyn til mennesker \citep[135]{pettit99}.
Dyr og økosystemer kan aldri bli borgere og omfattes derfor ikke av samme
beskyttelse og rettigheter som det borgere gjør.
Det republikanske svaret er ikke å utvide borgerbegrepet, men å vise
hvordan vern av miljø og tiltak for å forhindre irreversible klimaendringer kan
begrunnes med utgangspunkt i frihet som ikke-dominans.
En stat som fremmer frihet som ikke-dominans må ta hensyn til alt som kan
reduseres til en form for herredømme.
Å vise at økologiske hensyn kan formuleres som spørsmål om frihet og herredømme,
er å vise at en slik stat må ta hensyn til dem.
To resonnementer kan illustrere hvordan slike begrunnelser kan se ut:
miljøødeleggelse som herredømme og dominans inn i fremtiden.

\subsection{Miljøødeleggelse som herredømme}

Inngrep i miljøet som går utover andre og som skjer uten at disse andres
interesser blir tatt hensyn til er en form for herredømme
\citep[138]{pettit99}: det vil innebære en vilkårlig forverring av en
tredjeparts valgsituasjon.
Formulert på denne måten er dette for alle praktiske formål identisk med Mills
``no harm''-prinsipp: miljøødeleggelse er bare utillatelig så lenge det har
(materielle) omkostninger for en annen (menneskelig) aktør, i alle andre
tilfeller er det ingen begrensninger.

Den vesentlige forskjellen ligger i at republikanismen ikke bare ser på faktiske
inngrep, men også potensielle inngrep.
Skaden inntreffer ikke i det noen hugger ned et tre eller raserer et jorde, men
er der allerede i det at slik aktivitet i det hele tatt er mulig.
Frihet som ikke-dominans krever skranker mot vilkårlig ødeleggelse av miljøet
overhodet.
Under dette paradigmet er negative eksternaliteter, også urealiserte
eksternaliteter, ikke tillatelige.
Et brudd på dette kan naturligvis kompenseres økonomisk, men i en tilnærmet
idealsituasjon vil det legges mer vekt på å hindre i forkant enn å reparere i
etterkant.

Dette åpner for ``defensive'' tiltak hvor bevisbyrden alltid vil ligge på den som
vil utføre inngrepet, og hvor det vil være mange stopp-punkter i maskineriet som
til sist vil lede frem til en beslutning om hvordan et gitt naturområde skal
brukes.
Langt på vei er dette resonnementet helt i tråd med \citet{barry99}.

\subsection{Dominans fra graven}

Å kunne begrunne miljøvern er ikke uvesentlig, men resonnementet kan ikke
utvides til klimaendringer.
I motsetning til ødeleggelser av bestemte økosystemer, er effektene på det
globale klimasystemet spredt utover en så lang tidshorisont at forholdet mellom
årsaker og virkninger blir uklart.
Hensynet til fremtidige generasjoner er en nærliggende begrunnelse for å hindre
denne type endringer.

Nøkkelen til en republikansk variant av dette resonnementet ligger i
forestillingen om ``dominans fra graven'' \citep[587--588]{pettit96}.
\citet{pettit96} bruker dette begrepet om tilfeller hvor tradisjonene, eller
de eksisterende lovene, virker diskriminerende.
Det kan i slike tilfeller være vanskelig å si at noen nålevende aktør utøver
herredømme gjennom disse lovene.
Lovene i seg selv representerer like fullt herredømme, fordi de som en gang
innstiftet dem gjorde det uten å ta hensyn til de som nå rammes, de dominerer
fra graven.
De som i dag er i stand til å endre disse lovene, uten å gjøre det, utøver et
indirekte herredømme \citep[587]{pettit96}.
På samme måte kan man si at lover som innebærer klimaendringer for fremtidige
generasjoner er en form for dominans fra graven.

For å begrunne at dette ikke er holdbart kan man resonnere som følger.
Hvis jeg kan ``dominere inn i fremtiden'', betyr det også at jeg kan
``domineres fra graven''.
Ergo er det rasjonelt å ønske å leve i et samfunn tuftet på et lovverk som
hindret forrige generasjon fra å dominere den generasjonen du selv tilhører.
Videre, siden dominans ett sted kontaminerer hele systemet vil jeg også innse at
et samfunn som tillater dominans inn i fremtiden er et dominerende samfunn.
Statlig intervensjon for hindre dette er legitimt og kollektivt rasjonelt.
Ideen om generasjonsfellesskap kan gis en substansiell begrunnelse med
henvisning til frihet som ikke-dominans, og krever ikke spesiell appell til det
nære og kjære (se bl.a. \citet{eckersley06}.)
Igjen vil dette i praksis legge opp til defensive mekanismer, som
føre-var-prinsippet og usikkerhetsprinsippet, som hos \citet{barry99}.

\section{Et omriss av en grønn republikk}

Den grønne republikken vil ikke i vesentlig grad skille seg fra det
nyrepublikanske prosjektet \citep[kap.5, kap.6]{pettit99} hva gjelder
institusjonell utforming.
Staten og lovene vil ha en avgjørende rolle, og fremming av frihet som
ikke-dominans er deres eneste legitime mål.
Som vist kan miljø- og klimaproblemer omformuleres til spørsmål om herredømme,
og derfor er også en stat som fremmer frihet som ikke-dominans fullt ut i stand
til å være økologisk rasjonell.

To vesentlige trekk ved måten en slik republikk kan innrettes er utvidet
anledning til å bruke lovverket som virkemiddel og institusjonelle mekanismer
som bremser aktivitet som anerkjennes som potensielt dominerende.

Ettersom lover ikke i seg selv reduserer frihet, er det legitimt å argumentere
for statlige inngrep for å oppnå mål i miljø- og klimapolitikken.
Dette kan ta form av lover som begrenser forbruk, ved hjelp av skatter og
avgifter, eller ved rent forbud.
Så fremt kriteriene for gode lover er tilfredsstilt (se \citet[590]{pettit96}
for en oversikt) må en en tilhenger av frihet som ikke-dominans anerkjenne
inngrepene som legitime.

Lover som er påvirket av sektorinteresser, via lobbyvirksomhet,
valgkampfinansiering eller andre former for manipulasjon, representerer
herredømme og er ikke legitime.
Eksisterende lover som har kommet til på denne måten må reverseres.
Næringsinteresser maskert som klima- og miljøpolitikk er overhodet ikke
tillatelig.

Hvor langt dette i praksis kan gå er ikke gitt på forhånd, men borgere av
republikken vil i prinsippet anerkjenne slike virkemidler og oppleve dem
som mindre problematiske enn innbyggere i en stat tuftet på et grunnleggende
forskjellig syn på lovenes rolle og virkning.

I \citet{barry99}s bidrag til grønn politisk teori beskrives institusjonelle
målsetninger og virkemidler som langt på vei likner det man kanskje
ville funnet i en hypotetisk grønn republikk, med den forskjell at nyrepublikanismen
erstatter ``borgerånd'' med tilslutning til frihet som fravær av herredømme
som det konstituerende prinsipp.
Usikkerhetsprinsippet og føre-var-prinsippet vil være naturlige komponenter i
enhver slik republikk, i tillegg til et mangfold av andre institusjonaliserte
skranker mot vilkårlige miljøødeleggelser og vilkårlig forverring av fremtidige
generasjoners valgsituasjon.
Det at dette kan forenes med en lang rekke andre verdier som pluralisme og
individuell autonomi, gjør en eventuell formulering av en mer fullendt grønn
republikansk platform godt rustet til å svare på den økologiske utfordringen.

\newpage
\bibliography{den_groenne_republikken}

\end{document}
